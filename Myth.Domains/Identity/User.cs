﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Myth.Core;

namespace Myth.Domains.Identity
{
    public class User
    {
        public static Expression<Func<User, bool>> ProcessedRule = x => x.Processed;

        [Obsolete("Use only for model binders and EF", true)]
        internal User() { }

        public User(string email, string avatar = null)
        {
            Email = email;
            Avatar = avatar;
        }

        public long UserId { get; set; }

        private string _email;
        public string Email
        {
            get { return _email; }
            set {
                if (string.IsNullOrEmpty(value)) {
                    throw new NullReferenceException($"{GetType().FullName}: Email. User with id: {UserId}, catch null.");
                }
                _email = value;
            }
        }

        public string Avatar { get; set; }

        public DateTimeOffset CreationDate { get; set; } = DateTimeOffset.Now;

        public bool Processed { get; set; }

        public virtual ICollection<SocialAccount>  SocialAccounts { get; set; } = new HashSet<SocialAccount>();

        public virtual ICollection<Role> Roles { get; set; } = new HashSet<Role>();
    }
}
