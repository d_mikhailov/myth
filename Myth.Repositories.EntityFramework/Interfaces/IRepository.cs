﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Myth.Repositories.EntityFramework.Interfaces
{
    public interface IRepository
    {
        IQueryable<T> GetAll<T>() where T : class;

        T Find<T>(long id) where T : class;

        Task<T> FindAsync<T>(long id) where T : class;

        T Add<T>(T dataObject) where T : class;

        void Remove<T>(T dataObject) where T : class;

        DbContext DbContext { get; }
    }
}
