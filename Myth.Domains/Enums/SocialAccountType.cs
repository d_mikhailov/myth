﻿namespace Myth.Domains.Enums
{
    public enum SocialAccountType
    {
        Myth = 0,
        Facebook,
        Google,
        Twitter
    }
}
