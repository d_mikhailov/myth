﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Myth.Repositories.EntityFramework.Interfaces;

namespace Myth.Repositories.EntityFramework
{
    public class Repository : IRepository
    {
        public Repository(DbContext dbContext)
        {
            if (dbContext == null) {
                throw new NullReferenceException();
            }
            DbContext = dbContext;
        }

        public IQueryable<T> GetAll<T>() where T : class
        {
            return DbContext.Set<T>();
        }

        public T Find<T>(long id) where T : class
        {
            return DbContext.Set<T>().Find(id);
        }

        public async Task<T> FindAsync<T>(long id) where T : class
        {
            return await DbContext.Set<T>().FindAsync(id);
        }

        public T Add<T>(T entity) where T : class
        {
            return DbContext.Set<T>().Add(entity);
        }

        public void Remove<T>(T entity) where T : class
        {
            DbContext.Set<T>().Remove(entity);
        }

        public DbContext DbContext { get; }
    }
}
