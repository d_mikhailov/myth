﻿using System;
using System.Collections.Generic;

namespace Myth.Domains.Identity
{
    public class Role
    {
        [Obsolete("Use only for model binders and EF", true)]
        internal Role() { }

        public Role(string roleName)
        {
            RoleName = roleName;
        }

        public long RoleId { get; set; }

        private string _roleName;
        public string RoleName
        {
            get { return _roleName; }
            set
            {
                if (string.IsNullOrEmpty(value)) {
                    throw new NullReferenceException($"{ GetType().FullName }: RoleName. Role with id: {RoleId}, catch null.");
                }
                _roleName = value;
            }
        }

        #region Many-to-Many relationship

        public virtual ICollection<User> Users { get; set; } = new HashSet<User>();

        #endregion
    }
}
