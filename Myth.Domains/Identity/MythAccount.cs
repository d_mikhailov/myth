﻿using System;
using Myth.Domains.Enums;

namespace Myth.Domains.Identity
{
    public class MythAccount : SocialAccount
    {
        public MythAccount(string socialAccountUid, SocialAccountType socialAccountType, string password) : base(socialAccountUid, socialAccountType)
        {

        }

        private string _password;

        public string Password
        {
            get { return _password; }
            set {
                if (string.IsNullOrEmpty(value)) {
                    throw new NullReferenceException();
                }
                _password = value;
            }
        }
    }
}