﻿using System;
using Myth.Domains.Enums;

namespace Myth.Domains.Identity
{
    public class SocialAccount
    {
        [Obsolete("Use only for model binders and EF", true)]
        protected internal SocialAccount() { }

        public SocialAccount(string socialAccountUid, SocialAccountType socialAccountType)
        {
            SocialAccountUid = socialAccountUid;
            SocialAccountType = socialAccountType;
        }

        public long SocialAccountId { get; set; }

        private string _socialAccountUid;
        public string SocialAccountUid
        {
            get { return _socialAccountUid; }
            set {
                if (string.IsNullOrEmpty(value)) {
                    throw new NullReferenceException($"{ GetType().FullName }: SocialAccountUid. User with id: {UserId}, catch null.");
                }
                _socialAccountUid = value;
            }
        }

        public DateTimeOffset AttachedDate { get; set; } = DateTimeOffset.Now;

        public SocialAccountType SocialAccountType { get; set; }

        #region One-to-Many relationship

        public long UserId { get; set; }

        public User User { get; set; }

        #endregion
    }
}
