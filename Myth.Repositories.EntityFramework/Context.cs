﻿using System.Data.Entity;
using Myth.Domains.Identity;

namespace Myth.Repositories.EntityFramework
{
    public class Context : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var userEntity = modelBuilder.Entity<User>();

            userEntity
                .HasMany(u => u.SocialAccounts)
                .WithRequired(s => s.User)
                .HasForeignKey(s => s.UserId);

            userEntity
                .HasMany(u => u.Roles)
                .WithMany(r => r.Users);
        }

        public DbSet<User> Users { get; set; }

        public DbSet<SocialAccount> SocialAccounts { get; set; }

        public DbSet<Role> Roles { get; set; }
    }
}
